﻿using System.Linq;
using System.Web.Http;
using BeerCrate.Api.ViewModels;

namespace BeerCrate.Api.Controllers
{
    public class AssortmentsController : BaseController
    {
        [Queryable]
        public IQueryable<Item> Get()
        {
            var entity = GetItems();
            return entity.Items.AsQueryable();
        }
    }
}