﻿using System.Xml.Serialization;

namespace BeerCrate.Api.ViewModels
{
    public class Item
    {
        [XmlElement(ElementName = "nr")]
        public string Id { get; set; }

        [XmlElement(ElementName = "Artikelid")]
        public string ItemId { get; set; }

        [XmlElement(ElementName = "Varnummer")]
        public string ProductId { get; set; }

        [XmlElement(ElementName = "Namn")]
        public string Name { get; set; }

        [XmlElement(ElementName = "Namn2")]
        public string Name2 { get; set; }

        [XmlElement(ElementName = "Prisinklmoms")]
        public string Price { get; set; }

        [XmlElement(ElementName = "Volymiml")]
        public string Volume { get; set; }

        [XmlElement(ElementName = "PrisPerLiter")]
        public string PricePerLiter { get; set; }

        [XmlElement(ElementName = "Saljstart")]
        public string StartAt { get; set; }

        [XmlElement(ElementName = "Slutlev")]
        public string EndAt { get; set; }

        [XmlElement(ElementName = "Varugrupp")]
        public string Category { get; set; }

        [XmlElement(ElementName = "Forpackning")]
        public string Packaging { get; set; }

        [XmlElement(ElementName = "Ursprung")]
        public string Origin { get; set; }

        [XmlElement(ElementName = "Ursprunglandnamn")]
        public string OriginCountry { get; set; }

        [XmlElement(ElementName = "Producent")]
        public string Producer { get; set; }

        [XmlElement(ElementName = "Leverantor")]
        public string Supplier { get; set; }

        [XmlElement(ElementName = "Argang")]
        public string Year { get; set; }

        [XmlElement(ElementName = "Provadargang")]
        public string YearTaste { get; set; }

        [XmlElement(ElementName = "Alkoholhalt")]
        public string Alcohol { get; set; }

        [XmlElement(ElementName = "Modul")]
        public string Module { get; set; }

        [XmlElement(ElementName = "Sortiment")]
        public string Assortment { get; set; }

        [XmlElement(ElementName = "Ekologisk")]
        public string Ecological { get; set; }

        [XmlElement(ElementName = "Koscher")]
        public string Koscher { get; set; }
    }
}