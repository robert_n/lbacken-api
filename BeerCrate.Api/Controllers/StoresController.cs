﻿using System.Linq;
using System.Web.Http;
using BeerCrate.Api.ViewModels;

namespace BeerCrate.Api.Controllers
{
    public class StoresController : BaseController
    {
        [Queryable]
        public IQueryable<Store> Get()
        {
            var entity = GetStores();
            return entity.Stores.AsQueryable();
        }
    }
}