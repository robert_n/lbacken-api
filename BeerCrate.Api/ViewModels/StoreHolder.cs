using System.Collections.Generic;
using System.Xml.Serialization;

namespace BeerCrate.Api.ViewModels
{
    [XmlRoot(ElementName = "ButikerOmbud")]
    public class StoreHolder
    {
        [XmlElement("ButikOmbud")]
        public List<Store> Stores { get; set; }
    }
}