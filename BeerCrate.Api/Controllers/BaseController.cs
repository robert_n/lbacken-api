﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.Http;
using System.Xml.Serialization;
using BeerCrate.Api.ViewModels;

namespace BeerCrate.Api.Controllers
{
    public abstract class BaseController : ApiController
    {
        private readonly string _path;

        private static long _lastCached;
        private static IDictionary<Type, object> _cache;

        protected BaseController()
        {
            _path = ConfigurationManager.AppSettings.Get("Path");

            if (_cache == null)
                _cache = new Dictionary<Type, object>();
        }

        protected ItemHolder GetItems()
        {
            var file = ConfigurationManager.AppSettings.Get("File.Items");
            return Get<ItemHolder>(Path.Combine(_path, file));
        }

        protected StoreHolder GetStores()
        {
            var file = ConfigurationManager.AppSettings.Get("File.Stores");
            return Get<StoreHolder>(Path.Combine(_path, file));
        }

        protected StoreItemsHolder GetStoreItems()
        {
            var file = ConfigurationManager.AppSettings.Get("File.StoreItems");
            return Get<StoreItemsHolder>(Path.Combine(_path, file));
        }

        private static T Get<T>(string path)
        {
            if (File.GetCreationTime(path).Ticks <= _lastCached && _cache.ContainsKey(typeof(T)))
                return (T) _cache[typeof (T)];

            T entity;

            using (var sr = new StreamReader(path))
            {
                var xml = new XmlSerializer(typeof (T));
                entity = (T) xml.Deserialize(sr);
            }

            if (_cache.ContainsKey(typeof (T)))
                _cache[typeof (T)] = entity;
            else
                _cache.Add(typeof (T), entity);

            _lastCached = File.GetCreationTime(path).Ticks;

            return entity;
        }
    }
}