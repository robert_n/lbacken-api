﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BeerCrate.Api.ViewModels
{
    [XmlRoot(ElementName = "artiklar")]
    public class ItemHolder
    {
        [XmlElement(ElementName = "skapad-tid")]
        public string CreatedAt { get; set; }

        [XmlElement("artikel")]
        public List<Item> Items { get; set; }
    }
}