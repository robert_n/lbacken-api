﻿using System.Web;
using System.Web.Http;
using BeerCrate.Api.App_Start;

namespace BeerCrate.Api
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            WebApiConfig.Register(GlobalConfiguration.Configuration);
        }
    }
}