using System.Collections.Generic;
using System.Xml.Serialization;

namespace BeerCrate.Api.ViewModels
{
    [XmlRoot(ElementName = "ButikArtikel")]
    public class StoreItemsHolder
    {
        [XmlElement("Butik")]
        public List<StoreItem> Stores { get; set; }
    }
}