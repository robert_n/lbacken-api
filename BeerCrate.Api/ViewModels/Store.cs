﻿using System.Xml.Serialization;

namespace BeerCrate.Api.ViewModels
{
    public class Store
    {
        [XmlElement(ElementName = "Nr")]
        public string Id { get; set; }

        [XmlElement(ElementName = "Typ")]
        public string Type { get; set; }

        [XmlElement(ElementName = "Address1")]
        public string Address1 { get; set; }

        [XmlElement(ElementName = "Address2")]
        public string Address2 { get; set; }

        [XmlElement(ElementName = "Address3")]
        public string Address3 { get; set; }

        [XmlElement(ElementName = "Address4")]
        public string Address4 { get; set; }

        [XmlElement(ElementName = "Address5")]
        public string Address5 { get; set; }

        [XmlElement(ElementName = "Telefon")]
        public string Phone { get; set; }

        [XmlElement(ElementName = "ButiksTyp")]
        public string StoreType { get; set; }

        [XmlElement(ElementName = "Tjanster")]
        public string Services { get; set; }

        [XmlElement(ElementName = "SokOrd")]
        public string Tags { get; set; }

        [XmlElement(ElementName = "Oppettider")]
        public string OpenAt { get; set; }

        [XmlElement(ElementName = "RT90x")]
        public string X { get; set; }

        [XmlElement(ElementName = "RT90y")]
        public string Y { get; set; }    }
}