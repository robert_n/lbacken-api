﻿using System.Linq;
using System.Web.Http;
using BeerCrate.Api.ViewModels;

namespace BeerCrate.Api.Controllers
{
    public class StoreItemsController : BaseController
    {
        [Queryable]
        public IQueryable<StoreItem> Get()
        {
            var entity = GetStoreItems();
            return entity.Stores.AsQueryable();
        }
    }
}