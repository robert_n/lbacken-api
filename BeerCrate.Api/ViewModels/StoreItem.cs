﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BeerCrate.Api.ViewModels
{
    public class StoreItem
    {
        [XmlAttribute("ButikNr")]
        public string Id { get; set; }

        [XmlElement(ElementName = "ArtikelNr")]
        public List<string> Items { get; set; }
    }
}